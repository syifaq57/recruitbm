import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';
import {
  createAppContainer,
  NavigationActions,
  NavigationAction,
} from 'react-navigation';

import 'react-native-gesture-handler';

import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';

import colors from './src/res/colors/index';
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/FontAwesome';

import Dashboard from './src/screen/Dashboard/Home/Dashboard';
import MusicPlayer from './src/screen/MusicPlayer/Home/MusicPlayer';
import ListMusic from './src/screen/ListMusic/Home/ListMusic';

var TabNav = createBottomTabNavigator(
  {
    Dashboard: {
      screen: Dashboard,
      navigationOptions: {
        title: 'Dashboard',
      },
    },

    MusicPlayer: {
      screen: MusicPlayer,
      navigationOptions: {
        title: 'Music Player',
      },
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName;
        let iconType;
        if (routeName === 'Dashboard') {
          iconName = 'folder-music';
        } else if (routeName === 'HomeProfile') {
          iconName = 'folder-music';
        }

        return <Icon name={iconName} size={20} color={tintColor} />;
      },
      headerVisible: false,
    }),
    initialRouteName: 'Dashboard',
    tabBarOptions: {
      activeTintColor: colors.bluegray,
      inactiveTintColor: 'gray',
      color: colors.bluegray,
      showIcon: true,
      showLabel: true,
    },
    animationEnabled: false,
    swipeEnabled: false,
    headerMode: 'none',
  },
);

const NavStack = createStackNavigator(
  {
    Dashboard: {
      screen: Dashboard,
    },
    ListMusic:{
      screen: ListMusic,
    },
  },
  // {
  //   ListMusic: {
  //     screen: ListMusic,
  //   },
  // },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

const DrawerLayout = createAppContainer(NavStack);
export default DrawerLayout;

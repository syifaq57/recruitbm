import React, { Component } from "react";
import { Platform, Image, TouchableOpacity } from "react-native";
import { Footer, FooterTab, Text, Button, Icon, View } from "native-base";
import colors from "../../res/colors";
// import console = require("console");


class CustomRadioButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statRadio: [],
      selected:[],
      lengthOption:this.props.option.length,
      option:this.props.option
    };
  }

  componentDidMount(){
    
    var selected=[]
    if(this.props.selected!=undefined){
      for(var i=0;i<this.state.lengthOption;i++){
        if(this.state.option[i].NAMA_LIST==this.props.selected){
          selected[i]=true
        }else{
          selected[i]=false
        }
        
      }
    }else{
      for(var i=0;i<this.state.lengthOption;i++){
        selected[i]=false
      }
    }
    this.setState({
      selected:selected
    })
    console.log(this.state.selected)
  }

  methodPress(data,index){
    var selected=[]
    for(var i=0;i<this.state.lengthOption;i++){  
      if (i==index){
        selected[i]=true
      }else{
        selected[i]=false
      }
    }
    this.setState({
      selected:selected
    })
    this.props.onPressMethod(data)
  }


  render() {
    // var statRadio=[]
    // for (let i=0;i<this.props.option.length;i++){
    //     statRadio[i]=false
    // }
    // this.setState({
    //     statRadio:statRadio
    // })
    return (
      <View style={{flexDirection: "column" }}>
      {this.state.option.map((data, index) => (
        <TouchableOpacity onPress={()=>this.methodPress(data,index)}>
        <View style={{flexDirection: "row",alignItems:'center',borderBottomColor:colors.gray05,borderTopColor:colors.gray05,borderBottomWidth:1,borderTopWidth:index==0?1:0,paddingBottom:5,paddingTop:5 }}>
          <View
            style={{
              height: 20,
              width: 20,
              borderRadius: 10,
              borderWidth: 2,
              marginRight:5,
              borderColor: colors.gray02,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            {this.state.selected[index] ? (
              <View
                style={{
                  height: 12,
                  width: 12,
                  borderRadius: 6,
                  backgroundColor: colors.green0
                }}
              />
            ) : null}
          </View>
          <Text style={{fontSize:11,marginRight:10,marginLeft:5}}> {data[this.props.displayData]}</Text>
        </View>
      </TouchableOpacity>
      ))}
    </View>
      
      
    );
  }
}

export default CustomRadioButton;

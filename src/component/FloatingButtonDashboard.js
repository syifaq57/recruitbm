import React, { Component } from "react";
import { View, Text, Image,StyleSheet,Dimensions,TouchableOpacity,AsyncStorage } from "react-native";
import {Card,Left,Right,Button} from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from "../../res/colors";
import ActionButton from 'react-native-action-button';

export default class FloatingButtonDashboard extends Component{

    navigateToScreen(route){
      
          this.props.navigation.navigate(route);
             
      }

    render(){
        return(            
            <ActionButton style={{marginBottom:-20}} size={50}  autoInactive={false}  buttonColor="rgba(13,214,132,1)">
                <ActionButton.Item buttonColor='#E9482F' title="Add Action" onPress={()=>(this.navigateToScreen("CreateDetailAction"))}>
                    <Icon name="exclamation-triangle" style={styles.actionButtonIcon} />
                </ActionButton.Item>
                <ActionButton.Item buttonColor='#3498db' title="Add Incident" onPress={() =>(this.navigateToScreen("CreateDetailIncident"))}>
                    <Icon name="plus-square" style={styles.actionButtonIcon} />
                </ActionButton.Item>
                <ActionButton.Item buttonColor='#DB1BBB' title="Add Observation" onPress={() =>(this.navigateToScreen("CreateDetailVPC"))}>
                    <Icon name="child" style={styles.actionButtonIcon} />
                </ActionButton.Item>
                <ActionButton.Item buttonColor='#F5F52A' title="Add Hazard" onPress={() =>(this.navigateToScreen("CreateDetailHazard"))}>
                    <Icon name="exclamation-triangle" style={styles.actionButtonIcon} />
                </ActionButton.Item>
            </ActionButton>  
            
            
        )
    }
}
const styles = StyleSheet.create({
    actionButtonIcon: {
      fontSize: 20,
      height: 22,
      color: 'white',
    },
  });
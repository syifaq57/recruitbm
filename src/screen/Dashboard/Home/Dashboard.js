/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter,
  PushNotificationIOS,
  ImageBackground,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  Card,
  CheckBox,
  Content,
} from 'native-base';

import {
  AdMobBanner,
  AdMobInterstitial,
  AdMobRewarded,
  PublisherBanner,
} from 'react-native-admob';

import {RectButton, BaseButton, BorderlessButton} from 'react-native-gesture-handler';

import colors from '../../../res/colors/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { color } from 'react-native-reanimated';

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleAll: true,
    };
  }

  componentDidMount() {
    // AdMobInterstitial.setAdUnitID('ca-app-pub-4681032570460349/9136530289');
    // AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    // AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
  }

  render() {
    return (
      <Container>
        <Header
          style={{backgroundColor:colors.black}}
          androidStatusBarColor={colors.blueDefault}
          >
          <View style={{width:'100%', alignItems:'center', justifyContent:'center'}}>
            <Text style={{fontSize:22,  fontWeight:'bold', color:colors.secondary}}>Audio Player </Text>
          </View>
        </Header>
        <View style={{alignItems: 'center'}}>
            <AdMobBanner
              adSize="banner"
              adUnitID="ca-app-pub-4681032570460349/3102439124"
              ref={el => (this._smartBannerExample = el)}
            />
        </View>
        <View style={{backgroundColor: colors.blueDefault, height: '100%'}}>
          <ScrollView>
            <View style={{width: '100%', alignItems: 'center'}}>
              <Card transparent style={{marginTop: 40}}>
                <View>
                  <Image
                    style={{width: 150, height: 150, borderRadius: 50}}
                    source={require('../../../res/images/tulus.jpeg')}
                  />
                </View>
              </Card>
              <View style={{marginVertical:40}}>
                <Text style={{color:'white', fontSize:20}}>Kumpulan Lagu Tulus</Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 20}}>
                <View
                  style={{
                    marginRight: 25,
                    justifyContent:'center',
                  }}>
                  <RectButton
                    style={{
                      padding: 15,
                      backgroundColor: colors.green03,
                      borderRadius: 50,
                      marginBottom: 5,
                    }}>
                    <Icon style={{color: colors.blueDefault}} name="information-outline" size={30} />
                  </RectButton>
                  <View style={{alignItems:'center'}}>
                    <Text style={{fontWeight:'bold', color:colors.secondary}}>About</Text>
                  </View>
                </View>
                <View
                  style={{
                    // borderWidth: 1,
                    borderColor: 'white',
                    borderRadius: 70,
                  }}>
                  <RectButton
                    style={{
                      padding: 25,
                      backgroundColor: colors.green01,
                      borderRadius: 70,
                      marginBottom: 5,
                    }}
                    onPress={this.props.navigation.navigate('ListMusic')}>
                    <Icon style={{color: colors.blueDefault}} name="play-circle" size={50} />
                  </RectButton>
                  <View style={{alignItems:'center'}}>
                    <Text style={{fontWeight:'bold', color:colors.secondary}}>Start Music</Text>
                  </View>
                </View>
                <View
                  style={{
                    marginLeft: 25,
                    justifyContent:'center',
                  }}>
                  <RectButton
                    style={{
                      padding: 15,
                      backgroundColor: colors.pink,
                      borderRadius: 50,
                      marginBottom: 5,
                    }}>
                    <Icon style={{color: colors.blueDefault}} name="exit-to-app" size={30} />
                  </RectButton>
                  <View style={{alignItems:'center'}}>
                    <Text style={{fontWeight:'bold', color:colors.secondary}}>Exit</Text>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
        <View style={{alignItems: 'center'}}>
        <PublisherBanner
              adSize="banner"
              validAdSizes={['banner', 'largeBanner', 'mediumRectangle']}
              adUnitID="ca-app-pub-4681032570460349/1329288497"
              ref={el => (this._adSizesExample = el)}
            />
        </View>
      </Container>

    );
  }
}
